<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
$route['testimonials/page(/:num)?']           = 'testimonials/index$1';
$route['testimonials/(/:num)?']           = 'testimonials/index$1';