<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @version 1.2
 * @author  Stanislav Stoynov
 * @link    http://stoynov.me
 *
 * Class Admin
 */
class Admin extends Admin_Controller
{

    public $data;

    public function __construct()
    {
        parent::__construct();

        $this->data = new stdClass();
        $this->lang->load('testimonials');
        $this->load->model('testimonials/testimonial_m');
        $this->load->driver('Streams');
    }

    /**
     * Name: index
     *
     * @return void
     */
    public function index()
    {
        $this->load->helper('text');
        $extra['title'] = 'testimonials';
        $extra['buttons'] = array(
            array(
                'label' => lang('global:edit'),
                'url' => 'admin/testimonials/edit/-entry_id-'
            ),
            array(
                'label' => lang('global:delete'),
                'url' => 'admin/testimonials/delete/-entry_id-',
                'confirm' => true
            )
        );
        $slugName = 'testimonials';
        $this->testimonial_m->getFolderId($slugName);
        
          

        $this->streams->cp->entries_table('testimonials', 'testimonials', 25, 'admin/testimonials/index', true, $extra);
    }

    /**
     * Name: create
     *
     * @return void
     */
    public function create()
    {
        $this->template->title(lang('testimonials:new'));

        $extra = array(
            'return' => 'admin/testimonials',
            'success_message' => lang('testimonials:submit_success'),
            'failure_message' => lang('testimonials:submit_failure'),
            'title' => lang('testimonials:new')
        );

        $this->streams->cp->entry_form('testimonials', 'testimonials', 'new', null, true, $extra);
    }

    /**
     * Name: edit
     *
     * @param int $id
     * @return void
     */
    public function edit($id = 0)
    {
        $this->template->title(lang('testimonials:edit'));

        $extra = array(
            'return' => 'admin/testimonials',
            'success_message' => lang('testimonials:submit_success'),
            'failure_message' => lang('testimonials:submit_failure'),
            'title' => lang('testimonials:edit')
        );

        $this->streams->cp->entry_form('testimonials', 'testimonials', 'edit', $id, true, $extra);
    }

    /**
     * Name: delete
     *
     * @param int $id
     * @return void
     */
    public function delete($id = 0)
    {
        $this->streams->entries->delete_entry($id, 'testimonials', 'testimonials');
        $this->session->set_flashdata('error', lang('testimonials:deleted'));
        redirect('admin/testimonials/');
    }

}
/* End of file admin.php */
