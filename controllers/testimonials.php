<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @version 1.2
 * @author  Stanislav Stoynov
 * @link    http://stoynov.me
 *
 * Class Testimonials
 */
class Testimonials extends Public_Controller
{
    public $data;

    public function __construct()
    {
        parent::__construct();
        $this->data = new stdClass();
        $this->lang->load('testimonials');
        $this->load->model('testimonial_m');
        $this->load->driver('Streams');
    }

    /**
     * Name: index
     *
     * @return void
     */
    public function index()
    {
        $params = array(
            'stream' => 'testimonials',
            'namespace' => 'testimonials',
            'paginate' => 'yes',
            'pag_segment' => 4
        );
        $this->data = new stdClass();
        $data->testimonials = $this->streams->entries->get_entries($params);
        
        $this->template
              ->title($this->module_details['name'])
              ->set_breadcrumb('Testimonials')
              ->build('index', $data);


    }

}

/* End of file testimonials.php */
