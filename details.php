<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * @version 1.2
 * @author  Stanislav Stoynov
 * @link    http://stoynov.me
 *
 * Class Module_Testimonials
 */
class Module_Testimonials extends Module
{
    public $version = '1.2';

    /**
     * Name: info
     *
     * @return array
     */
    public function info()
    {
        return array(
            'name' => array(
                'en' => 'Testimonials'
            ),
            'description' => array(
                'en' => 'Testimonials'
            ),
            'frontend' => true,
            'backend' => true,
            'menu' => 'content',
            'shortcuts' => array(
                'create' => array(
                    'name' => 'testimonials:new',
                    'uri' => 'admin/testimonials/create',
                    'class' => 'add'
                )
            )
        );
    }

    /**
     * Name: install
     *
     * @return bool
     */
    public function install()
    {
        $this->stream('add');
        return true;
    }

    /**
     * Name: uninstall
     *
     * @return bool
     */
    public function uninstall()
    {
        $this->stream('remove');

        $this->streams->utilities->remove_namespace('testimonials');
        return true;
    }

    /**
     * Name: upgrade
     *
     * @param $old_version
     * @return bool
     */
    public function upgrade($old_version)
    {
        return true;
    }

    /**
     * Name: stream
     *
     * @param string $action
     * @return void
     */
    public function stream($action = 'add')
    {
        
        $this->load->driver('Streams');
        $this->load->language('testimonials/testimonials');
        
        if($action == 'add')
        {
            // Add faqs streams
            if ( ! $this->streams->streams->add_stream(lang('testimonials:testimonials'), 'testimonials', 'testimonials', null, null)) return false;
            $this->load->library('files/files');
            $this->load->model('files/file_folders_m');
            $this->load->model('testimonials/testimonial_m');
              $slug = 'testimonials';
               $parent_id = 0;
               Files::create_folder($parent_id, 'Testimonials', 'local');
               $folder_id = $this->testimonial_m->getFolderId($slug);

            // Add some fields
            $fields = array(
                array(
                    'name' => 'Name',
                    'slug' => 'name',
                    'namespace' => 'testimonials',
                    'type' => 'text',
                    'extra' => array('max_length' => 200),
                    'assign' => 'testimonials',
                    'title_column' => true,
                    'required' => true,
                    'unique' => false
                ),
                array(
                    'name' => 'Company',
                    'slug' => 'company',
                    'namespace' => 'testimonials',
                    'type' => 'text',
                    'extra' => array('max_length' => 200),
                    'assign' => 'testimonials',
                    'title_column' => true,
                    'required' => false,
                    'unique' => false
                ),
                array(
                    'name' => 'Quote',
                    'slug' => 'quote',
                    'namespace' => 'testimonials',
                    'type' => 'textarea',
                    'assign' => 'testimonials',
                    'required' => true
                ),
                array(
                    'name' => 'Image',
                    'slug' => 'image',
                    'namespace' => 'testimonials',
                    'type' => 'image',
                    'extra' => array('allowed_types' => 'gif|jpg|png|JPG|jpeg|JPEG', 'folder' => $folder_id, 'resize_height' => '200', 'resize_width' => '200' ),      
                    'assign' => 'testimonials',
                    'required' => true
                ),
                array(
                    'name' => 'Featured',
                    'slug' => 'featured',
                    'namespace' => 'testimonials',
                    'type' => 'choice',
                    'extra' => array('choice_type' => 'radio', 'choice_data' => "0 : No\n 1 : Yes"),
                    'assign' => 'testimonials'
                    )
            );
           
            $this->streams->fields->add_fields($fields);
            $this->streams->streams->update_stream('testimonials', 'testimonials', array(
                'view_options' => array(
                    'id',
                    'company'
                )
            ));
        }
        else{
            $this->streams->utilities->remove_namespace('testimonials');
        }
    }

    /**
     * Name: help
     *
     * @return string
     */
    public function help()
    {
        return "No documentation has been added for this module.<br />Contact with me: stanislav@stoynov.me";
    }

}