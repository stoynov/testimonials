<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * @version 1.2
 * @author  Stanislav Stoynov
 * @link    http://stoynov.me
 *
 * Class testimonial_m
 */
class testimonial_m extends MY_Model {

   private static $db;

   public function __construct() {
      parent::__construct();
      $this->load->driver('Streams');
      self::$db = $this->db;
      $this->_table = 'testimonial';
   }

    /**
     * Name: getAll
     *
     * @param int $limit
     * @param string $orderBy
     * @return mixed
     * @static
     */
   public static function getAll($limit = 10, $orderBy = 'ASC') {
      $query = 'SELECT * FROM default_testimonials WHERE featured = 0 ORDER BY id ' . $orderBy . ' LIMIT ' . $limit;
      $return = self::$db->query($query, array('ob' => $orderBy, 'limit' => $limit))->result();
      return $return;
   }

    /**
     * Name: getFolderId
     *
     * @param $slug
     * @return mixed
     * @static
     */
   public static function getFolderId($slug) {
      $query = 'SELECT id FROM default_file_folders WHERE slug = "'.$slug.'"';
      $data = self::$db->query($query)->result();
      foreach ($data as $value) {
      	$myID = $value->id;
      }
      return $myID;
      
   }
}