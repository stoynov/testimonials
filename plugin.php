<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @version 1.2
 * @author  Stanislav Stoynov
 * @link    http://stoynov.me
 *
 * Class Plugin_Testimonials
 */
class Plugin_Testimonials extends Plugin
{
    /**
     * Name: Testimonials
     *
     * @return mixed
     */
    public function Testimonials()
    {

        $testimonials = $this->db->
        get_where('testimonials', array('featured' => 1))
            ->result_array();

        return $testimonials;
    }

    /**
     * Name: Featured
     *
     * @return string
     */
    public function Featured()
    {
        $testimonials = $this->db->
        get_where('testimonials_testimonials', array('featured' => 'Yes'))
            ->result_array();

        $output = "";

        foreach ($testimonials as $testimonial) {

            $output .= "<section class=\"wibble\">";
            $output .= "<h4>&ldquo;" . $testimonial['quote'] . "&rdquo;</h4>";
            $output .= "<p><span>" . $testimonial['company'] . "</span></p>";
            $output .= '</section>';
        }

        return $output;
    }
}