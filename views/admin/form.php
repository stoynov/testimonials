<section class="title">
	<h4><?php echo lang('testimonials:'.$this->method); ?></h4>
</section>

<section class="item">

	<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
		
		<div class="form_inputs">
	
		<ul>
			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="company"><?php echo lang('testimonials:company'); ?> <span>*</span></label>
				<div class="input"><?php echo form_input('company', set_value('company', $company), 'class="width-15"'); ?></div>
			</li>
			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="name"><?php echo lang('testimonials:name'); ?> <span>*</span></label>
				<div class="input"><?php echo form_input('name', set_value('name', $name), 'class="width-15"'); ?></div>
			</li>
			
			<li class="<?php echo alternator('', 'even'); ?>">
                <label for="image"><?php echo lang('testimonials:image'); ?> <span>*</span></label>
                <div class="input"><?php echo form_upload('file', set_value('image', $image), 'class="width-15"'); ?></div>
            </li>

		
		</ul>
		
		</div>
		
		<div class="buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )); ?>
		</div>
		
	<?php echo form_close(); ?>

</section>