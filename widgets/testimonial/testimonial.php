<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @version 1.2
 * @author  Stanislav Stoynov
 * @link    http://stoynov.me
 *
 * Class Widget_Testimonial
 */
class Widget_Testimonial extends Widgets
{
	public $title		= array(
		'en' => 'Testimonial',
	);
	public $description	= array(
		'en' => 'Display Testimonials selected as featured as widget.',
	);

	public $author		= 'Stanislav Stoynov';
	public $website		= 'http://stoynov.me';
	public $version		= '1.2';

	public $fields = array(
		array(
			'field' => 'limit',
			'label' => 'Number of Testimonials',
		)
	);

	public function form($options)
	{
		$options['limit'] = ( ! empty($options['limit'])) ? $options['limit'] : 5;

		return array(
			'options' => $options
		);
	}

	public function run($options)
	{
		$this->lang->load('testimonials/testimonials');

        $this->load->driver('Streams');

		// sets default number of posts to be shown
		$options['limit'] = ( ! empty($options['limit'])) ? $options['limit'] : 5;

		$params = array(
            'stream' => 'testimonials',
            'namespace' => 'testimonials',
            'paginate' => 'no',
            'where' => "featured = '1'",
            'limit' => $options['limit']
        );

        $testimonials = $this->streams->entries->get_entries($params);
		// returns the variables to be used within the widget's view
		return array('testimonial_widget' => $testimonials);
	}
}
